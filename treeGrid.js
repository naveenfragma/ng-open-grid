(function(){
    var data = [];
    for(var j = 0;j<10;j++)
    {
        data.push([]);
        for(var k = 0;k<10;k++)
        {
            data[j].push(j+"."+k);
        }
    }
    var content = [
            {
                name: 'node1', id: 1,
                children: [
                    { 
                        name: 'child1', id: 2,
                        children : [
                            {name : "nested child1" , id : 6},
                            {name : "nested child2" , id : 7}
                        ]
                    },
                    { name: 'child2', id: 3 }
                ]
            },
            {
                name: 'node2', id: 4,
                children: [
                    { name: 'child3', id: 5 }
                ]
            }
        ];
    var app = angular.module("treeApp",[]);
    app.directive("gridView",function(){
       return{
         restrict : "E",
         templateUrl : "gridDirective.html",
         controller : function(){
           var scope = this;
           this.content=data;
           this.editIndex = [-1,-1];
            //toggle editing
            this.toggle = function(x,y){
              scope.editIndex = [x,y];  
            };
            this.isSet = function(x,y){
              return JSON.stringify(scope.editIndex) == JSON.stringify([x,y]);  
            };
             
            //SORT BY INDEX
             this.sortStatus = 1;
             this.sortBy = function(index,status){
                scope.sortStatus *= -1;
               for(var i=0;i<scope.content.length;i++){
                   var min = i;
                   for(var j=i+1;j<scope.content.length;j++){
                       if(scope.sortStatus*scope.content[min][index] > scope.sortStatus*scope.content[j][index]){
                           min = j;
                       }
                   }
                   var temp = scope.content[i];
                   scope.content[i] = scope.content[min];
                   scope.content[min] = temp;
               }
             };
         },
         controllerAs : "gridCtrl"
       };
    });
    
    app.directive("treeView",function(){
       return{
         restrict : 'E',
         templateUrl : 'treeDirective.html',
         controller : function(){
            var scope = this;
            this.treeData = [];
             
             //load to Tree data
            this.loadTreeData = function(source,level,visible){
                for(var item in source){
                    scope.treeData.push({
                        name:source[item].name,
                        level:level,
                        visible:visible,
                        showingNextLevel:false,
                        containsChildren:true
                    });
                    if(source[item].children != null)
                        scope.loadTreeData(source[item].children,level+1,false);
                    else
                        scope.treeData[scope.treeData.length-1].containsChildren = false;
                }
            };
            this.loadTreeData(content,0,true);
        
             //helper function to print spaces
            this.getNumber = function(num){
                  return new Array(num);  
            };
             
            //show Next Level
            this.showNextLevel = function(index){
              scope.treeData[index].showingNextLevel = true;
              for(var i=index+1;i<scope.treeData.length;i++){
                if(scope.treeData[index].level >= scope.treeData[i].level)
                    break;
                if(scope.treeData[i].level == (scope.treeData[index].level+1)){
                    scope.treeData[i].visible = true;
                }
              }
            };
             
            //hide next level(nested levels too)
            this.hideNextLevel = function(index){
              scope.treeData[index].showingNextLevel = false;
              for(var i=index+1;i<scope.treeData.length;i++){
                if(scope.treeData[index].level >= scope.treeData[i].level)
                    break;
                scope.treeData[i].visible = false;
                scope.treeData[i].showingNextLevel = false;
              }
            };
             
            //expand all nodes
            this.expandAll = function(){
                for(var i=0;i<scope.treeData.length;i++){
                    scope.treeData[i].visible=true;
                    scope.treeData[i].showingNextLevel = true;
                }
            };
             
            //collapse all nodes
             this.collapseAll = function(){
               for(var i=0;i<scope.treeData.length;i++){
                   //level-0 :true , else :false
                   scope.treeData[i].visible = scope.treeData[i].level?false:true;
                   scope.treeData[i].showingNextLevel = false;
               }  
             };
         },
         controllerAs : "treeCtrl"
       };
    });
    
})();